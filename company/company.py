from private import constants
from iroha import Iroha, IrohaGrpc, IrohaCrypto
from flask import jsonify
import pymongo
from secrets import token_hex
from json.decoder import JSONDecoder
from datetime import datetime

class CompanyHelper:
    def __init__(self):
        self.__iroha_network = IrohaGrpc(constants.iroha_network)
        self.__iroha_client = Iroha(constants.iroha_client)


    def verify(self, account_id: str) -> bool:
        """
        This method returns true if the account is a company or false if it's not.

        :param str account_id: The unique id of the account to check
        """
        
        # Create query
        new_query = self.__iroha_client.query(
            'GetAccount',
            account_id=account_id
        )

        # Sign query and send to network
        IrohaCrypto.sign_query(new_query, constants.private_key)
        iroha_response = self.__iroha_network.send_query(new_query)

        # Get response
        response = iroha_response.account_response

        if not response:
            return False
        elif 'company' in response.account_roles:
            return True
        else:
            return False


    def check_private_key(self, account_id: str, private_key: str) -> bool:
        """
        Verifies if the private key belongs to the account initiating a request.

        :param str account_id: The unique account id
        :param str private_key: The private key associated with the account
        """

        # Create iroha query to get signatories
        signatories_query = self.__iroha_client.query(
            'GetSignatories',
            account_id=account_id 
        )

        # Sign and send query to network as the orchestrator
        IrohaCrypto.sign_query(signatories_query, constants.private_key)
        iroha_response = self.__iroha_network.send_query(signatories_query)

        # Get response
        response = iroha_response.signatories_response

        if not response:
            return False
        else:
            keys = list(map(lambda x: x.decode('utf-8'), response.keys))
            if IrohaCrypto.derive_public_key(private_key).decode('utf-8') in keys:
                return True
            else:
                return False


class Company:
    def __init__(self):
        self.__iroha_network = IrohaGrpc(constants.iroha_network)
        self.__iroha_client = Iroha(constants.iroha_client)
        self.__helper = CompanyHelper()
        self.__mongo_instance = pymongo.MongoClient(constants.mongo_instance)
        self.__orchestrator_db = self.__mongo_instance['orchestrator_db']
        self.jobs_collection = self.__orchestrator_db['jobs']
        self.companies_collection = self.__orchestrator_db['companies']
    
    def set_info(self, account_id: str, company_name: str):
        """
        This method sets extra info to the company, all on the blockchain.

        :param str account_id: The company account
        :param str company_name: The full name of the company.
        """

        # Check if the account is a company account.
        is_company = self.__helper.verify(account_id)

        if not is_company:
            return jsonify({
                'status': 'error',
                'msg': 'This account is not a company account'
            }), 401
        
        # Create a new transaction
        new_tx = self.__iroha_client.transaction([
            self.__iroha_client.command(
                'SetAccountDetail',
                account_id=account_id,
                key="company_name",
                value=company_name
            )
        ])

        # Sign and send transaction to network
        IrohaCrypto.sign_transaction(new_tx, constants.private_key)
        self.__iroha_network.send_tx(new_tx)

        # Follow transaction status
        return self.__follow_set_company_info(new_tx)


    def get_info(self, account_id: str):
        """
        This method retrieves extra-data about a company from the blockchain

        :parm str account_id: Account to get extra-data for.
        """

        # Create new query
        new_query = self.__iroha_client.query(
            'GetAccount',
            account_id=account_id        
        )

        # Sign query and send it to network
        IrohaCrypto.sign_query(new_query, constants.private_key)
        iroha_response = self.__iroha_network.send_query(new_query)

        # Get response
        response = iroha_response.account_response

        if not response or response == '':
            return jsonify({
                'status': 'error',
                'msg': "Company details not found"
            }), 404
        else:
            decoder = JSONDecoder()
            info = decoder.decode(response.account.json_data)[constants.iroha_client]
            return jsonify({
                'status': 'success',
                'msg': 'Info retrieved sucessfully',
                'info': info
            }), 200


    def get_public_info(self, company_id: str):
        """
        This method can be used by 3rd parties to get public data about a company

        :param str company_id: The id of the company on the Iroha blockchain
        """

        # Verify if the company is indeed a company
        company_helper = CompanyHelper()
        if not company_helper.verify(company_id):
            return jsonify({
                'status': 'error',
                'msg': 'Company does not exist!'
            }), 404

        # Create new query
        new_query = self.__iroha_client.query(
            'GetAccount',
            account_id=company_id        
        )

        # Sign query and send it to network
        IrohaCrypto.sign_query(new_query, constants.private_key)
        iroha_response = self.__iroha_network.send_query(new_query)

        # Get response
        response = iroha_response.account_response

        if not response or response == '':
            return jsonify({
                'status': 'error',
                'msg': "Company details not found"
            }), 404
        else:
            try:
                decoder = JSONDecoder()
                info = decoder.decode(response.account.json_data)[constants.iroha_client]
                return jsonify({
                    'status': 'success',
                    'msg': 'Info retrieved sucessfully',
                    'info': info
                }), 200
            except:
                return jsonify({
                    'status': 'success',
                    'msg': 'Info retrieved sucessfully',
                    'info': {'company_name': ''}
                }), 200


    def __follow_set_company_info(self, transaction):
        """
        Follows the status update of a 'SetAccountDetail' transaction on the blockchain.
        Returns relevant http responses depending on the status of the transaction.

        :param transaction: An iroha transaction with one 'SetAccountDetail' command
        """

        for status in self.__iroha_network.tx_status_stream(transaction):
            # Stateless error
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({
                    'status': 'error',
                    'msg': 'Wrong info details set!'
                }), 403
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server error
                if status[2] == 1:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Internal server error!'
                    }), 500
                # No such permission
                if status[2] == 2:
                    return jsonify({
                        'status': 'error',
                        'msg': "This account doesn't have persmission to set company details!"
                    }), 401
                # No such account
                if status[2] == 3:
                    return jsonify({
                        'status': 'error',
                        'msg': "No company with this id was found!"
                    }), 404
            elif status[0] == "COMMITTED":
                return jsonify({
                    'status': 'success',
                    'msg': 'Information set sucessfully'
                }), 200


    def publish_job(self, account_id: str, private_key: str, job):
        """
        Generates a job token and transfers it to the local orchestrator.

        :param str account_id: The unique id of the company
        :param str private_key: The private key of the company account
        :param job: A dict containing all the necessary data about a job
        """

        iroha_company_client = Iroha(account_id)
        domain = account_id.split('@')[1]

        # Generate a 32-bit token for the job
        token = token_hex(32)

        # Create transactions and transaction batch
        transacions = [
            iroha_company_client.transaction([
                iroha_company_client.command(
                    'AddAssetQuantity',
                    asset_id='job#' + domain,
                    amount="1"
                )
            ]),

            iroha_company_client.transaction([
                iroha_company_client.command(
                    'TransferAsset',
                    src_account_id=account_id,
                    dest_account_id=constants.iroha_client,
                    asset_id='job#'+domain,
                    description=token,
                    amount="1"
                )
            ])
        ]

        iroha_company_client.batch(transacions, atomic=True)

        # Sing and send transaction to network
        for transaction in transacions:
            IrohaCrypto.sign_transaction(transaction, private_key)
        self.__iroha_network.send_txs(transacions)

        # Listen to all transactions for status updates
        mint_job_answer, mint_job_code = self.__follow_mint_job_tx(transacions[0])
        if mint_job_code != 200:
            return mint_job_answer

        transfer_job_answer, transfer_job_code = self.__follow_transfer_job_tx(transacions[1])
        if transfer_job_code != 200:
            return transfer_job_answer

        # Save the job in the mongoDB cache
        self.jobs_collection.insert_one({
            'name': job['name'],
            'description': job['description'],
            'city': job['city'],
            'study': job['study'],
            'type': job['type'],
            'salary': job['salary'],
            'skills': job['skills'],
            'skill_levels': job['skill_levels'],
            'company': account_id,
            '_id': token
        })

        # Insert the job in the company document
        public_key = IrohaCrypto.derive_public_key(private_key).decode('utf-8')

        try:
            new_job = {"job_id": token, "applicants": []}
            company = self.companies_collection.find_one({'public_key': public_key})
            if 'jobs' not in company:
                company['jobs'] = []
            company['jobs'].append(new_job.copy())
            self.companies_collection.replace_one({'public_key': public_key}, company)
        except Exception as ex:
            print(ex)
            return jsonify({
                'status': 'error',
                'msg': "Can't publish the job!"
            }), 500

        return jsonify({
            'status': 'success',
            'msg': 'Successfully published job!'
        }), 200


    def delete_job(self, account_id: str, job_id: str):
        """
        Deletes the job from Mongo and burns one job token from the orchestrator

        :param str account_id: The company id
        :param str job_id: The job id
        """

        # Verify the company
        comapny_helper = CompanyHelper()
        if not comapny_helper.verify(account_id):
            return jsonify({'status': 'error', 'msg': 'Company not found'}), 404
        
        # Delete job from jobs collection and from company document
        try:
            self.jobs_collection.delete_one({'_id': job_id})
            company = self.companies_collection.find_one({'name': account_id.split('@')[1]})
            for job in company['jobs']:
                if job['job_id'] == job_id:
                    company['jobs'].remove(job)
                    break
            self.companies_collection.replace_one({'name': account_id.split('@')[1]}, company)
        except Exception as ex:
            return jsonify({'status': 'error', 'msg': 'Could not delete job'}), 500

        # Burn a job token

        # Create transaction
        burn_tx = self.__iroha_client.transaction([
            self.__iroha_client.command(
                'SubtractAssetQuantity',
                asset_id='job#' + account_id.split('@')[1],
                amount='1'
            )
        ])

        # Sign and send transaction
        IrohaCrypto.sign_transaction(burn_tx, constants.private_key)
        self.__iroha_network.send_tx(burn_tx)

        return self.__follow_job_burn_tx(burn_tx)


    def __follow_job_burn_tx(self, transaction):
        """
        Follows the status update of a burning transaction on the blockchain.

        :param transaction: An iroha transaction with a 'SubtractAssetQuantity' command
        """

        for status in self.__iroha_network.tx_status_stream(transaction):
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({'status': 'error', 'msg': 'Bad format on burning transaction'}), 403
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server error
                if status[2] == 1:
                    return jsonify({'status': 'error', 'msg': 'Internal server error'}), 500
                # No permissions
                elif status[2] == 2:
                    return jsonify({'status': 'error', 'msg': "This orchestrator can't burn assets"}), 403
                # No such asset
                elif status[2] == 3:
                    return jsonify({'status': 'error', 'msg': 'Asset was not found'}), 404
                # Not enough balance
                elif status[2] == 4:
                    return jsonify({'status': 'error', 'msg': 'There are no jobs to delete'}), 403
            elif status[0] == "COMMITTED":
                return jsonify({'status': 'success', 'msg': 'Job deleted sucessfully'}), 200


    def register_applicant(self, account_id: str, company_id: str, job_id: str):
        """
        This method is used to register a new developer as applicant for a job

        :param str account_id: The developer that applies for the job
        :param str company_id: The company that posted the job
        :param str job_id: The job
        """

        # 1. Verify if the company exists
        company_helper = CompanyHelper()
        if not company_helper.verify(company_id):
            return jsonify({
                'status': 'error',
                'msg': 'Company not found!'
            }), 404
        
        # 2. Verify there is a job with this id, published by this company

        # 2.a) Get the company object from mongo
        company = self.companies_collection.find_one(
            {'name': company_id.split('@')[1]}
        )

        # 2.b) Get the jobs and check them sequentially
        jobs = company['jobs']
        published = False
        job_index = 0
        for job in jobs:
            if job['job_id'] == job_id:
                published = True
                break
            job_index += 1
        
        # 3. Update the company object and re-upload to mongo
        if not published:
            return jsonify({
                'status': 'error',
                'msg': 'Job not found!'
            }), 404
        else:
            if account_id not in company['jobs'][job_index]['applicants']:
                company['jobs'][job_index]['applicants'].append(account_id)
            else:
                return jsonify({
                    'status': 'success',
                    'msg': 'Already applied for this job!'
                }), 200
            try:
                self.companies_collection.replace_one(
                    {'name': company_id.split('@')[1]},
                    company
                )
            except Exception as e:
                return jsonify({
                    'status': 'error',
                    'msg': "Couldn't apply for job!"
                })
            return jsonify({
                'status': 'success',
                'msg': 'Successfully applied for the job!'
            }), 200


    def __follow_mint_job_tx(self, transaction):
        """
        Follows status updates of job asset creations

        :param transaction: An iroha transaction issuing a job token
        """

        for status in self.__iroha_network.tx_status_stream(transaction):
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({
                    'status': 'error',
                    'msg': 'Wrong job asset details set!'
                }), 403
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server error
                if status[2] == 1:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Internal server error!'
                    }), 500
                # No such permissions
                elif status[2] == 2:
                    return jsonify({
                        'status': 'error',
                        'msg': "This company can't issue new jobs."
                    }), 401
                # No such asset
                elif status[2] == 3:
                    return jsonify({
                        'status': 'error',
                        'msg': "The job asset was not found in this domain."
                    }), 404
                # Summation overflow
                elif status[2] == 4:
                    return jsonify({
                        'status': 'error',
                        'msg': "The amount of jobs exceeds the maximum number of jobs for this company."
                    }), 403
            elif status[0] == "COMMITTED":
                return jsonify({
                    'status': 'success',
                    'msg': "Job created successfully!"
                }), 200


    def __follow_transfer_job_tx(self, transaction):
        """
        Follows the status update of an iroha transaction that transfers a job token to an orchestrator.

        :param transaction: The transaction.
        """

        for status in self.__iroha_network.tx_status_stream(transaction):
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({
                    'status': 'error',
                    'msg': "Couldn't publish the job on the market! Format error."
                }), 403
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server error
                if status[2] == 1:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Internal server error!'
                    }), 500
                # No such permission
                elif status[2] == 2:
                    return jsonify({
                        'status': 'error',
                        'msg': "This company doesn't have permissions to publish a job!"
                    }), 401
                # No such source account
                elif status[2] == 3:
                    return jsonify({
                        'status': 'error',
                        'msg': "Source account not found!"
                    }), 404
                # No such destination account
                elif status[2] == 4:
                    return jsonify({
                        'status': 'error',
                        'msg': "Destination account not found!"
                    }), 404
                # No such asset found
                elif status[2] == 5:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Asset id not found!'
                    }), 404
                # Not enough balance
                elif status[2] == 6:
                    return jsonify({
                        'status': 'error',
                        'msg': "Trying to publish more jobs than created"
                    }), 500
                # Too much asset to transfer
                elif status[2] == 7:
                    return jsonify({
                        'status': 'error',
                        'msg': "Trying to publish too many jobs!"
                    }), 403
            elif status[0] == "COMMITTED":
                return jsonify({
                    'status': 'success',
                    'msg': 'Job publihed successfully!'
                }), 200


    def get_applicant_info(self, dev_id):
        """
        Gets extra info about an applicant from the blockchain + the description from mongo
        """

        # Write a new query for the blockchain
        new_query = self.__iroha_client.query(
            'GetAccount',
            account_id=dev_id
        )

        # Sign the query with the Instrumentality key
        IrohaCrypto.sign_query(new_query, constants.private_key)

        # Send the query to the network and get an answer
        iroha_response = self.__iroha_network.send_query(new_query)
        account_details = iroha_response.account_response.account.json_data

        decoder = JSONDecoder()

        try:
            info = decoder.decode(account_details)[constants.iroha_client]
        except KeyError:
            return jsonify({'msg': 'No data found', 'status': 'error'}), 404

        try:
            user = self.__mongo_instance['orchestrator_db']['users'].find_one({'_id': dev_id})
            info['description'] = user['description']
        except:
            print('Unable to load description for ' + dev_id)


        return info, 200


    def hire(self, company_id: str, token: str, dev_id: str, job_id: str):

        # Check the company
        company_helper = CompanyHelper()
        if not company_helper.verify(company_id):
            return jsonify({'status': 'error', 'msg': 'This account is not a company'}), 401
        
        # Create backups in case blockchain transaction fails
        curr_job_bkp = {}
        job_bkp = {}

        # Check user jobs and move current job to past jobs
        try:
            dev = self.__orchestrator_db['users'].find_one({'_id': dev_id})
            # Check 'old_jobs' key
            if "old_jobs" not in dev:
                dev['old_jobs'] = []
            # Give developer the job
            # If dev is currently employed then make his current job obsolete
            if "current_job" in dev and dev['current_job'] != {}:
                current_job = dev['current_job']
                curr_job_bkp = dev['current_job'].copy()
                current_job['end_date']=datetime.timestamp(datetime.now())
                dev['old_jobs'].append(dev['current_job'].copy())
                dev['current_job'] = {}
            # Assign new job
            current_job = self.jobs_collection.find_one({'_id': job_id})
            current_job['start_date'] = datetime.timestamp(datetime.now())
            dev['current_job'] = current_job.copy()

            # Write changes back to mongo
            self.__orchestrator_db['users'].find_one_and_replace({'_id': dev_id}, dev)
            # Transfer user to company
            if dev_id not in self.companies_collection.find_one({'name': company_id.split('@')[1]})['users']:
                self.companies_collection.update_one({'name': company_id.split('@')[1]}, {"$push": {'users': dev_id}})
            # Take job out of the job market
            job_bkp = self.jobs_collection.find_one_and_delete({'_id': job_id})

            # Delete job from company posted jobs
            self.companies_collection.update_one({'name': company_id.split('@')[1]}, {"$pull": {"jobs": {"job_id": job_bkp['_id']}}})
        except:
            return jsonify({'status': 'error', 'msg': 'Could not complete hiring process'}), 500
        
        # Transfer job token
        hire_tx = self.__iroha_client.transaction([
            self.__iroha_client.command(
                'TransferAsset',
                src_account_id=constants.iroha_client,
                dest_account_id=dev_id,
                asset_id='job#' + company_id.split('@')[1],
                description=job_id,
                amount='1'
            )
        ])

        # Sign transaction and send to network
        IrohaCrypto.sign_transaction(hire_tx, constants.private_key)
        self.__iroha_network.send_tx(hire_tx)

        hire_response, hire_code = self.__follow_hire_tx(hire_tx)

        if hire_code != 200:
            # Revert mongo transactions
            # Remove dev from company:
            self.companies_collection.update_one({'name': company_id.split('@')[1]}, {"$pull": {'users': {'_id': dev_id}}})
            # Remove current backed up job from old_jobs
            if curr_job_bkp != {}:
                self.__orchestrator_db['users'].update_one({'_id': dev_id}, {"$pull": {"old_jobs": {"job_id": curr_job_bkp['_id']}}})
                self.__orchestrator_db['users'].update_one({'_id': dev_id}, {"$set": {"current_job": curr_job_bkp}})
            # Restore job to jobs collection
            self.jobs_collection.insert_one(job_bkp)
        
        # Remove developer from all job applications
        self.__remove_applicant_from_everywhere(dev_id)

        return hire_response, hire_code


    def __remove_applicant_from_everywhere(self, dev_id: str):
        companies = self.companies_collection.find({})

        for company in companies:
            modified = False
            
            for job in company['jobs']:
                if dev_id in job['applicants']:
                    modified = True
                    job['applicants'].remove(dev_id)
            
            if modified:
                self.companies_collection.find_one_and_replace({'name': company['name']}, company)


    def create_task(self, company_id: str, private_key: str, dev_id: str, task: dict):
        """
            Creates a new task token on the chain and transfers it to the asigned developer.
            The developer can later redeem this task token for skill rewards.

            The task object is asigned to the user on Mongo, where it has an attached id. To redeem this task,
            a developer needs the task id and a blockchain token issued in the domain of his company.

            Create Process: new task arrives -> id is attached -> token is minted in the company domain -> the token is transfered to the dev
            Redeem Process: dev provides a task token that has been minted in his company domain and a task id -> after validation, dev receives the rewards
            specified in the task body
        """

        # Mint task token
        iroha_company_client = Iroha(company_id)
        mint_tx = iroha_company_client.transaction([
            iroha_company_client.command(
                'AddAssetQuantity',
                asset_id="task#" + company_id.split('@')[1],
                amount='1'
            )
        ])

        transfer_tx = iroha_company_client.transaction([
            iroha_company_client.command(
                'TransferAsset',
                src_account_id=company_id,
                dest_account_id=constants.iroha_client,
                asset_id="task#" + company_id.split('@')[1],
                description="Task assigned",
                amount='1'
            )
        ])

        # Sign and send transaction to network
        IrohaCrypto.sign_transaction(mint_tx, private_key)
        IrohaCrypto.sign_transaction(transfer_tx, private_key)
        self.__iroha_network.send_tx(mint_tx)
        self.__iroha_network.send_tx(transfer_tx)

        mint_task_answer, mint_task_code = self.__follow_create_task_tx(mint_tx)
        transfer_task_answer, transfer_task_code = self.__follow_transfer_task_tx(transfer_tx)

        # Check transaction status
        if mint_task_code != 200:
            return mint_task_answer, mint_task_code
        elif transfer_task_code != 200:
            return transfer_task_answer, transfer_task_code
        else:
            # Create new task and asign to dev
            try:
                task['id'] = token_hex(64)
                self.__orchestrator_db['users'].update_one({'_id': dev_id}, {"$push": {'tasks': task}})
                return jsonify({'status': 'success', 'msg': 'Task created and assigned sucessfully'}), 200
            except:
                return jsonify({'status': 'error', 'msg': "Couldn't store task!"}), 500


    def __follow_create_task_tx(self, transaction):
        for status in self.__iroha_network.tx_status_stream(transaction):
            print("Task creation: " + str(status))
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({
                    'status': 'error',
                    'msg': 'Wrong task asset details set!'
                }), 403
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server error
                if status[2] == 1:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Internal server error!'
                    }), 500
                # No such permissions
                elif status[2] == 2:
                    return jsonify({
                        'status': 'error',
                        'msg': "This company can't create new tasks."
                    }), 401
                # No such asset
                elif status[2] == 3:
                    return jsonify({
                        'status': 'error',
                        'msg': "The task asset was not found in this domain."
                    }), 404
                # Summation overflow
                elif status[2] == 4:
                    return jsonify({
                        'status': 'error',
                        'msg': "The amount of tasks exceeds the maximum number of tasks for this company."
                    }), 403
            elif status[0] == "COMMITTED":
                return jsonify({
                    'status': 'success',
                    'msg': "Task created successfully!"
                }), 200


    def __follow_transfer_task_tx(self, transaction):
        for status in self.__iroha_network.tx_status_stream(transaction):
            print("Task assignment: " + str(status))
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({
                    'status': 'error',
                    'msg': "Couldn't assign the task! Format error."
                }), 403
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server error
                if status[2] == 1:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Internal server error!'
                    }), 500
                # No such permission
                elif status[2] == 2:
                    return jsonify({
                        'status': 'error',
                        'msg': "This orchestrator doesn't have permissions to assign tasks!"
                    }), 401
                # No such source account
                elif status[2] == 3:
                    return jsonify({
                        'status': 'error',
                        'msg': "Source account not found!"
                    }), 404
                # No such destination account
                elif status[2] == 4:
                    return jsonify({
                        'status': 'error',
                        'msg': "Destination account not found!"
                    }), 404
                # No such asset found
                elif status[2] == 5:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Asset id not found!'
                    }), 404
                # Not enough balance
                elif status[2] == 6:
                    return jsonify({
                        'status': 'error',
                        'msg': "Trying to assign more tasks than created"
                    }), 500
                # Too much asset to transfer
                elif status[2] == 7:
                    return jsonify({
                        'status': 'error',
                        'msg': "Trying to assign too many tasks!"
                    }), 403
            elif status[0] == "COMMITTED":
                return jsonify({
                    'status': 'success',
                    'msg': 'Task publihed successfully!'
                }), 200


    def __follow_hire_tx(self, transaction):
        """
        Follows the status update of an iroha transaction that transfers a job token to a developer.

        :param transaction: The transaction.
        """

        for status in self.__iroha_network.tx_status_stream(transaction):
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({
                    'status': 'error',
                    'msg': "Couldn't complete the hiring process! Format error."
                }), 403
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server error
                if status[2] == 1:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Internal server error!'
                    }), 500
                # No such permission
                elif status[2] == 2:
                    return jsonify({
                        'status': 'error',
                        'msg': "This orchestrator can't transfer jobs!"
                    }), 401
                # No such source account
                elif status[2] == 3:
                    return jsonify({
                        'status': 'error',
                        'msg': "Source account not found!"
                    }), 404
                # No such destination account
                elif status[2] == 4:
                    return jsonify({
                        'status': 'error',
                        'msg': "Destination account not found!"
                    }), 404
                # No such asset found
                elif status[2] == 5:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Asset id not found!'
                    }), 404
                # Not enough balance
                elif status[2] == 6:
                    return jsonify({
                        'status': 'error',
                        'msg': "Trying to transfer more jobs than created"
                    }), 500
                # Too much asset to transfer
                elif status[2] == 7:
                    return jsonify({
                        'status': 'error',
                        'msg': "Trying to transfer too many jobs!"
                    }), 403
            elif status[0] == "COMMITTED":
                return jsonify({
                    'status': 'success',
                    'msg': 'Developer hired successfully!'
                }), 200


    def get_applicant_wallet(self, dev_id: str):

        get_wallet_qr = self.__iroha_client.query(
            'GetAccountAssets',
            account_id=dev_id,
        )

        IrohaCrypto.sign_query(get_wallet_qr, constants.private_key)
        iroha_response = self.__iroha_network.send_query(get_wallet_qr)

        # Get response
        response = iroha_response.account_assets_response

        if not response:
            return jsonify({'status': 'error', 'msg': 'Could not retrieve wallet'}), 500
        
        assets = response.account_assets
        skills = []

        for asset in assets:
            skills.append({'asset_id': asset.asset_id, 'balance': asset.balance})

        return jsonify({'status': 'success', 'assets': skills}), 200
