from iroha import IrohaCrypto, Iroha, IrohaGrpc
from flask import jsonify
from private import constants
import pymongo

class Maker:
    """
    Maker is responsible of asset creation and asset minting.
    """

    def __init__(self):
        self.__iroha_client = Iroha(constants.iroha_client)
        self.__iroha_network = IrohaGrpc(constants.iroha_network)
        self.__mongo_instance = pymongo.MongoClient(constants.mongo_instance)
        self.__orchestrator_db = self.__mongo_instance['orchestrator_db']
        self.assets_collection = self.__orchestrator_db['assets']

    
    def create_asset(self, asset_name: str, precision: int):
        """
        This method generates a single new asset in the instrumentality domain.

        :param str asset_name: Unique name for the new asset.
        :param str precision: The number of decimals allowed after dot.

        :returns: Json response + http status code
        """

        # Create a new transaction
        asset_create_tx = self.__iroha_client.transaction([
            self.__iroha_client.command(
                'CreateAsset',
                asset_name=asset_name,
                domain_id="instrumentality",
                precision=precision
            )
        ])

        # Sign transaction and send to network
        IrohaCrypto.sign_transaction(asset_create_tx, constants.private_key)
        self.__iroha_network.send_tx(asset_create_tx)

        return self.__follow_asset_creation(asset_create_tx, asset_name)


    def mint_asset(self, asset_name: str, amount: float):
        """
        This method issues a new amount of a specified asset in the instrumentality domain.

        :param str asset_name: The name of the asset you want to issue
        :param float amount: The amount to be issued

        :returns: JSON + http status code
        """

        # Create a transaction to mint asset
        mint_asset_tx = self.__iroha_client.transaction([
            self.__iroha_client.command(
                'AddAssetQuantity',
                asset_id=asset_name + '#instrumentality',
                amount=str(amount)
            )
        ])

        # Sign and send transaction to network
        IrohaCrypto.sign_transaction(mint_asset_tx, constants.private_key)
        self.__iroha_network.send_tx(mint_asset_tx)

        return self.__follow_asset_minting(mint_asset_tx)


    def __follow_asset_creation(self, transaction, asset_name: str):
        """
        This method follows the status of a transaction that creates an asset
        and returns a (JSON, status_code) tuple

        :param transaction: An iroha transaction that has a 'CreateAsset' command
        :param asset_name: The name of the new asset
        """

        # Follow status of asset creation
        for status in self.__iroha_network.tx_status_stream(transaction):
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({
                    'status': 'error',
                    'msg': 'Wrong asset details sent'
                }), 403
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal error
                if status[2] == 1:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Internal server error!'
                    }), 500
                elif status[2] == 2:
                    # No permission
                    return jsonify({
                        'status': 'error',
                        'msg': "This account can't create assets!"
                    }), 401
                elif status[2] == 3:
                    # No domain found
                    return jsonify({
                        'status': 'error',
                        'msg': "There's no domain with this name!"
                    }), 404
                elif status[2] == 4:
                    # Duplicate asset
                    return jsonify({
                        'status': 'error',
                        'msg': "This asset already exists on this domain!"
                    }), 409
            elif status[0] == "COMMITTED":
                self.assets_collection.insert_one({
                    'name': asset_name
                })
                return jsonify({
                    'status': 'success',
                    'msg': 'Asset created successfully!'
                }), 200


    def __follow_asset_minting(self, transaction):
        """
        This method tracks the status of an asset minting transaction

        :param transaction: An Iroha transaction that does asset minting
        
        :returns: JSON + http status code
        """

        for status in self.__iroha_network.tx_status_stream(transaction):
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({
                    'status': 'error',
                    'msg': 'Wrong asset details sent!'
                }), 403
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server errror
                if status[2] == 1:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Internal server error!'
                    }), 500
                # No permissions
                elif status[2] == 2:
                    return jsonify({
                        'status': 'error',
                        'msg': 'This account cannot mint assets!'
                    }), 401
                # No such asset
                elif status[2] == 3:
                    return jsonify({
                        'status': 'error',
                        'msg': 'There is no asset with this name in the domain!'
                    }), 404
                # Summation overflow
                elif status[2] == 4:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Issuing anymore assets would exceed the maximum number of allowed quantity!'
                    }), 403
            elif status[0] == "COMMITTED":
                return jsonify({
                    'status': 'success',
                    'msg': 'Asset quantity added successfully!'
                })
                
                
