
from flask import Flask, request, send_from_directory, jsonify
from flask_cors import CORS
from account import account
from account.auth_helper import AuthHelper
from json.encoder import JSONEncoder
from json.decoder import JSONDecoder
from werkzeug.utils import secure_filename
import os
import glob
from pathlib import Path
from orchestrator.orchestrator import Orchestrator
from skill_maker.maker import Maker
from company.company import CompanyHelper, Company
import pymongo
from private import constants

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = './Profiles'
app.config['MAX_CONTENT_LENGTH'] = 4 * 1024 * 1024

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'webp', 'svg'}

cors = CORS(app, resources={
    r"/*":
        {
            "origins": ['http://localhost:4200', 'http://localhost:8080', 'http://136.244.81.12:8080']
        }
})

auth_helper = AuthHelper()
orchestrator = Orchestrator()
maker = Maker()
company_helper = CompanyHelper()
company = Company()


mongo = pymongo.MongoClient(constants.mongo_instance)


def allowed_file(filename: str):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/account/create/<acc_name>')
def account_create(acc_name: str):
    return account.create(acc_name, "instrumentality")


@app.route('/account/auth', methods=['POST'])
def account_auth():
    req_body = request.get_json()

    return account.auth(req_body['username'], req_body['privateKey'])


@app.route('/account/verify', methods=['POST'])
def account_verify():
    req_body = request.get_json()

    return account.verify(req_body['username'], req_body['token'])


@app.route('/account/set/details', methods=['POST'])
def account_set_details():
    req_body = request.get_json()

    return account.set_details(req_body)


@app.route('/account/get', methods=['PUT'])
def account_get():
    req_body = request.get_json()

    return account.get_details(req_body['username'], req_body['token'])


@app.route('/account/set/image', methods=['POST'])
def account_set_image():
    response = {}
    encoder = JSONEncoder()

    body = request.form

    username = body.get('username')
    token = body.get('token')
    image = request.files['image']

    # Creating the folder to store files
    p = Path(app.config['UPLOAD_FOLDER'])
    p.mkdir(exist_ok=True)

    if image.filename == '':
        response['msg'] = "No file was sent for upload"
        response['status'] = 'error'

        return encoder.encode(response), 404
    if image and allowed_file(image.filename):
        if not auth_helper.validate_token(username, token):
            response['msg'] = "Invalid token!"
            response['status'] = 'error'
            return encoder.encode(response), 401

        try:
            fn = glob.glob(app.config['UPLOAD_FOLDER'] + "/" + username + "-profile.*")[0]
            if fn:
                os.remove(fn)
        except Exception as ex:
            print('No profile image')

        filename = secure_filename(image.filename)
        image.save(os.path.join(app.config['UPLOAD_FOLDER'], username + '-' + 'profile.' + filename.rsplit('.', 1)[1].lower()))

        response['msg'] = 'Image uploaded successfully'
        response['status'] = 'success'
        return encoder.encode(response), 200
    else:
        response['msg'] = 'Image format not recognized'
        response['status'] = 'error'
        return encoder.encode(response), 403


@app.route('/account/get/image', methods=['PUT'])
def account_get_image():
    response = {}
    body = request.get_json()

    username = body['username']
    token = body['token']

    if not auth_helper.validate_token(username, token):
        response['msg'] = 'Invalid token'
        response['status'] = 'error'
        return jsonify(response), 401
    else:
        try:
            filename = glob.glob(app.config['UPLOAD_FOLDER'] + "/" + username + "-profile.*")[0]
        except IndexError:
            return jsonify({'msg': 'No profile picture', 'status': 'success'}), 404
        return send_from_directory('.', filename)


@app.route("/is_orchestrator", methods=['POST'])
def is_orchestrator():
    body = request.get_json()

    account_id = body['account']
    token = body['token']

    if account.auth_helper.validate_token(account_id, token):
        is_o = account.orchestrator_helper.verify(account_id)
        if is_o:
            return jsonify({'status': 'success', 'msg': True}), 200
        else:
            return jsonify({'status': 'success', 'msg': False}), 200
    else:
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401


@app.route("/is_company", methods=['POST'])
def is_company():
    body = request.get_json()

    account_id = body['account_id']
    token = body['token']

    if account.auth_helper.validate_token(account_id, token):
        is_c = company_helper.verify(account_id)
        if is_c:
            return jsonify({'status': 'success', 'msg': True}), 200
        else:
            return jsonify({'status': 'success', 'msg': False}), 200
    else:
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401


@app.route("/company/create", methods=['POST'])
def company_create():
    body = request.get_json()

    account_id = body['account']
    token = body['token']
    company_name = body['companyName']

    if not (account.auth_helper.validate_token(account_id, token) and account.orchestrator_helper.verify(account_id)):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401
    else:
        return orchestrator.create_company(company_name)


@app.route("/company/set_info", methods=['POST'])
def company_set_info():
    body = request.get_json()

    account_id = body['account_id']
    token = body['token']
    company_name = body['company_name']

    if not account.auth_helper.validate_token(account_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401
    else:
        return company.set_info(account_id, company_name)


@app.route("/company/get_info", methods=['POST'])
def company_get_info():
    body = request.get_json()

    account_id = body['account_id']
    token = body['token']

    if not account.auth_helper.validate_token(account_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401
    else:
        return company.get_info(account_id) 


@app.route("/company/get_public_info", methods=['POST'])
def get_public_company_info():
    body = request.get_json()

    account_id = body['account_id']
    token = body['token']
    company_id = body['company_id']

    if not account.auth_helper.validate_token(account_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401
    else:
        return company.get_public_info(company_id)


@app.route("/company/get/applicant", methods=['POST'])
def get_applicant_info():
    body = request.get_json()

    company_id = body['company_id']
    token = body['token']
    dev_id = body['dev_id']

    # Check token validity
    if not auth_helper.validate_token(company_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401
    
    # Check if it's actually a company making this request
    if not company_helper.verify(company_id):
        return jsonify({'status': 'error', 'msg': 'This account is not a company'}), 401

    # Check if the dev applied for at least one job at this company
    applied = False
    try:
        company_obj = company.companies_collection.find_one({'name': company_id.split('@')[1]})
        for job in company_obj['jobs']:
            if applied:
                break
            for applicant in job['applicants']:
                if applicant == dev_id:
                    applied = True
                    break
    except:
        return jsonify({'status': 'error', 'msg': 'Could not check applicant'}), 500
    
    # Finally get info and return it
    return company.get_applicant_info(dev_id)


@app.route("/company/get/applicant_photo", methods=['POST'])
def get_applicant_photo():
    body = request.get_json()

    company_id = body['company_id']
    token = body['token']
    dev_id = body['dev_id']

    # Check token validity
    if not auth_helper.validate_token(company_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401
    
    # Check if it's actually a company making this request
    if not company_helper.verify(company_id):
        return jsonify({'status': 'error', 'msg': 'This account is not a company'}), 401

    # Check if the dev applied for at least one job at this company
    applied = False
    try:
        company_obj = company.companies_collection.find_one({'name': company_id.split('@')[1]})
        for job in company_obj['jobs']:
            if applied:
                break
            for applicant in job['applicants']:
                if applicant == dev_id:
                    applied = True
                    break
    except:
        return jsonify({'status': 'error', 'msg': 'Could not check applicant'}), 500
    
    # Finally get photo and return it
    try:
        filename = glob.glob(app.config['UPLOAD_FOLDER'] + "/" + dev_id + "-profile.*")[0]
    except IndexError:
        return jsonify({'msg': 'No profile picture', 'status': 'success'}), 404
    return send_from_directory('.', filename)


@app.route("/company/get/applicant_jobs", methods=['POST'])
def get_applicant_jobs():
    body = request.get_json()

    company_id = body['company_id']
    token = body['token']
    dev_id = body['dev_id']

    # Check token validity
    if not auth_helper.validate_token(company_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401
    
    # Check if it's actually a company making this request
    if not company_helper.verify(company_id):
        return jsonify({'status': 'error', 'msg': 'This account is not a company'}), 401

    # Check if the dev applied for at least one job at this company
    applied = False
    try:
        company_obj = company.companies_collection.find_one({'name': company_id.split('@')[1]})
        for job in company_obj['jobs']:
            if applied:
                break
            for applicant in job['applicants']:
                if applicant == dev_id:
                    applied = True
                    break
    except:
        return jsonify({'status': 'error', 'msg': 'Could not check applicant'}), 500
    
    # Finally get the jobs

    try:
        company_obj = company.companies_collection.find_one({'name': company_id.split('@')[1]})
        jobs = []
        for job in company_obj['jobs']:
            if dev_id in job['applicants']:
                new_job = company.jobs_collection.find_one({'_id': job['job_id']})
                jobs.append(new_job)
        return jsonify({'status': 'success', 'jobs': jobs}), 200
    except:
        return jsonify({'status': 'error', 'msg': 'Could not get jobs for this user'}), 500


@app.route("/company/hire", methods=['POST'])
def hire():
    body = request.get_json()

    company_id = body['company_id']
    token = body['token']
    job_id = body['job_id']
    dev_id = body['dev_id']

    # Check the token
    if not auth_helper.validate_token(company_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401

    return company.hire(company_id, token, dev_id, job_id)


@app.route("/company/publish_job", methods=['POST'])
def publish_job():
    body = request.get_json()

    decoder = JSONDecoder()

    # Get all the necessary parts from request body
    private_key = body['private_key']
    account_id = body['account_id']
    job = decoder.decode(body['job'])

    return company.publish_job(account_id, private_key, job)


@app.route('/company/get/image', methods=['POST'])
def company_get_image():
    body = request.get_json()

    account_id = body['account_id']
    token = body['token']
    company = body['company']

    if not auth_helper.validate_token(account_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials!'}), 401
    elif not company_helper.verify(company):
        return jsonify({'status': 'error', 'msg': 'The company does not exist'}), 401
    else:
        try:
            filename = glob.glob(app.config['UPLOAD_FOLDER'] + "/" + company + "-profile.*")[0]
        except IndexError:
            return jsonify({'msg': 'No profile picture', 'status': 'error'}), 404
        return send_from_directory('.', filename)


@app.route("/company/get/employees", methods=['POST'])
def get_employees():
    body = request.get_json()

    company_id = body['company_id']
    token = body['token']

    if not auth_helper.validate_token(company_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials!'}), 401
    elif not company_helper.verify(company_id):
        return jsonify({'status': 'error', 'msg': 'The company does not exist'}), 401
    
    # Get the users from the company
    try:
        employees = []
        users = company.companies_collection.find_one({'name': company_id.split('@')[1]})['users']
        for user in users:
            employees.append(mongo['orchestrator_db']['users'].find_one({'_id': user}))
        return jsonify({'status': 'success', 'employees': employees}), 200
    except:
        return jsonify({'status': 'error', 'msg': "Couldn't retrieve the list of employees"}), 500


@app.route("/company/create_task", methods=['POST'])
def create_task():
    body = request.get_json()

    company_id = body['company_id']
    token = body['token']
    private_key = body['private_key']
    dev_id = body['dev_id']
    task = body['task']

    if not auth_helper.validate_token(company_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials!'}), 401
    elif not company_helper.verify(company_id):
        return jsonify({'status': 'error', 'msg': 'The company does not exist'}), 401

    return company.create_task(company_id, private_key, dev_id, task)


@app.route("/asset/create", methods=['POST'])
def asset_create():
    body = request.get_json()

    account_id = body['account']
    token = body['token']
    asset_name = body['asset_name']
    precision = body['precision']

    if not (account.auth_helper.validate_token(account_id, token) and account.orchestrator_helper.verify(account_id)):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401
    else:
        return maker.create_asset(asset_name, precision)


@app.route("/asset/mint", methods=['POST'])
def asset_mint():
    body = request.get_json()

    if not (account.auth_helper.validate_token(body['account'], body['token']) and account.orchestrator_helper.verify(body['account'])):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials'}), 401
    else:
        return maker.mint_asset(body['asset_name'], body['amount'])


@app.route("/asset/get/list")
def get_asset_list():
    assets = list(map((lambda x: x['name']), list(maker.assets_collection.find({}))))

    return jsonify({'assets': assets}), 200


@app.route("/job/get/list")
def get_job_list():
    jobs = list(company.jobs_collection.find({}))

    return jsonify({"jobs": jobs}), 200


@app.route("/job/apply", methods=['POST'])
def apply_for_job():
    body = request.get_json()

    account_id = body['account_id']
    token = body['token']
    company_id = body['company_id']
    job_id = body['job_id']

    if not account.auth_helper.validate_token(account_id, token):
        return jsonify({
            'status': 'error',
            'msg': 'Invalid credentials!'
        }), 401
    else:
        return company.register_applicant(account_id, company_id, job_id)


@app.route("/job/get/company_list", methods=['PUT'])
def get_company_job_list():
    body = request.get_json()
    company_id = body['company_id']

    try:
        jobs = list(company.jobs_collection.find({'company': company_id}))
        return jsonify({
            "jobs": jobs
        }), 200
    except Exception as ex:
        print(ex)
        return jsonify({
            'status': 'error',
            'msg': "Can't get the job list"
        }), 500


@app.route("/job/delete", methods=['POST'])
def delete_job():
    body = request.get_json()

    account_id = body['account_id']
    token = body['token']
    job_id = body['job_id']

    if not auth_helper.validate_token(account_id, token):
        return jsonify({
            'status': 'error',
            'msg': 'Invalid credentials'
        }), 401
    else:
        return company.delete_job(account_id, job_id)


@app.route("/job/get/applicants", methods=['POST'])
def get_job_applicants():
    body = request.get_json()

    account_id = body['account_id']
    token = body['token']
    job_id = body['job_id']

    if not auth_helper.validate_token(account_id, token):
        return jsonify({
            'status': 'error',
            'msg': 'Invalid credentials'
        }), 401

    if not company_helper.verify(account_id):
        return jsonify({
            'status': 'error',
            'msg': 'This account is not a company!'
        }), 401

    applicants = []

    try:
        company_obj = company.companies_collection.find_one({'name': account_id.split('@')[1]})
        for job in company_obj['jobs']:
            if job['job_id'] == job_id:
                applicants = job['applicants']
                break
        return jsonify({'status': 'success', 'applicants': applicants}), 200
    except:
        return jsonify({'status': 'error', 'applicants': applicants}), 500


@app.route("/task/get/list", methods=['POST'])
def get_task_list():
    body = request.get_json()

    account_id = body['account_id']
    token = body['token']

    if not auth_helper.validate_token(account_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials!'}), 401

    try:
        user = mongo['orchestrator_db']['users'].find_one({'_id': account_id})
        return jsonify({'status': 'success', 'tasks': user['tasks']}), 200
    except:
        return jsonify({'status': 'error', 'msg': 'Could not retrieve the tasks'}), 500


@app.route("/task/mark_done", methods=['POST'])
def mark_task_done():
    body = request.get_json()

    account_id = body['account_id']
    token = body['token']
    task_id = body['task_id']

    if not auth_helper.validate_token(account_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials!'}), 401
    
    return orchestrator.cashTask(account_id, task_id)


@app.route("/account/get/wallet", methods=['POST'])
def get_account_assets():
    body = request.get_json()

    account_id = body['account_id']
    token = body['token']
    
    if not auth_helper.validate_token(account_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials!'}), 401

    return orchestrator.get_dev_wallet(account_id)


@app.route("/company/get/applicant_wallet", methods=['POST'])
def get_applicant_wallet():
    body = request.get_json()

    company_id = body['company_id']
    dev_id = body['dev_id']
    token = body['token']

    # Check token
    if not auth_helper.validate_token(company_id, token):
        return jsonify({'status': 'error', 'msg': 'Invalid credentials!'}), 401

    # Check dev is actively applying for a position at this company
    company_obj = mongo['orchestrator_db']['companies'].find_one({'name': company_id.split('@')[1]})
    is_applicant = False
    for job in company_obj['jobs']:
        if dev_id in job['applicants']:
            is_applicant = True
            break
    
    if is_applicant:
        return company.get_applicant_wallet(dev_id)
    else:
        return jsonify({'status': 'error', 'msg': 'This developer is has not applied to any of your jobs!'}), 404


if __name__ == '__main__':
    app.run()
