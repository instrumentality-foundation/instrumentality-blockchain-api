from private import constants
from iroha import Iroha, IrohaGrpc, IrohaCrypto
from flask import jsonify
import pymongo


class OrchestratorHelper:
    def __init__(self):
        self.__role = 'orchestrator'
        self.__iroha_network = IrohaGrpc(constants.iroha_network)
        self.__iroha_client = Iroha(constants.iroha_client)

    def verify(self, account_id) -> bool:
        """
        This method can tell you if an account is an orchestrator or not

        :param str account_name: The unique name of an account
        :return: True or False
        """

        # Create query
        get_account_qry = self.__iroha_client.query(
            'GetAccount',
            account_id=account_id
        )

        # Sign and send query
        IrohaCrypto.sign_query(get_account_qry, constants.private_key)
        iroha_response = self.__iroha_network.send_query(get_account_qry)

        # Get response
        response = iroha_response.account_response

        if not response:
            return False
        else:
            # Check the roles
            roles = response.account_roles
            if 'orchestrator' in roles:
                return True
            else:
                return False

class Orchestrator:
    def __init__(self):
        self.__iroha_network = IrohaGrpc(constants.iroha_network)
        self.__iroha_client = Iroha(constants.iroha_client)
        self.__mongo_instance = pymongo.MongoClient(constants.mongo_instance)
        self.__orchestrator_db = self.__mongo_instance['orchestrator_db']
        self.companies_collection = self.__orchestrator_db['companies']


    def create_company(self, company_name: str):
        """
        Creates a new account and adds the right permissions to it.
        It also creates a new domain for that company and a RoboVault
        where all the skill tokens will be stored.

        :param str company_name: The name of the company to be created
        
        :return: Tuple of json + status code
        """

        # Create key pair for the new company
        company_priv = IrohaCrypto.private_key().decode('utf-8')
        company_pub = IrohaCrypto.derive_public_key(company_priv).decode('utf-8')

        # Create public key for robo_vault. No need to get the private key.
        robo_vault_pub = IrohaCrypto.derive_public_key(IrohaCrypto.private_key().decode('utf-8')).decode('utf-8')
        
        # Create transactions and transaction batch
        transactions = [
            self.__iroha_client.transaction([
                self.__iroha_client.command(
                    'CreateDomain',
                    domain_id=company_name,
                    default_role='developer'
                )
            ]),

            self.__iroha_client.transaction([
                self.__iroha_client.command(
                    'CreateAccount',
                    account_name=str.lower(company_name),
                    domain_id=company_name,
                    public_key=company_pub
                )
            ]),

            self.__iroha_client.transaction([
                self.__iroha_client.command(
                    'CreateAsset',
                    asset_name="job",
                    domain_id=company_name,
                    precision=0
                )
            ]),

            self.__iroha_client.transaction([
                self.__iroha_client.command(
                    'CreateAsset',
                    asset_name="task",
                    domain_id=company_name,
                    precision=0
                )
            ]),

            self.__iroha_client.transaction([
                self.__iroha_client.command(
                    'AppendRole',
                    account_id=str.lower(company_name) + '@' + company_name,
                    role_name="company"
                ),

                self.__iroha_client.command(
                    'DetachRole',
                    account_id=str.lower(company_name) + '@' + company_name,
                    role_name="developer"
                )
            ])
        ]

        self.__iroha_client.batch(transactions, atomic=True)

        # Sign and send transactions to network
        for transaction in transactions:
            IrohaCrypto.sign_transaction(transaction, constants.private_key)
        self.__iroha_network.send_txs(transactions)

        # Listen to all transactions for an answer
        domain_answer, domain_code = self.__follow_domain_creation(transactions[0])
        if domain_code != 200:
            return domain_answer, domain_code

        company_answer, company_code  = self.__follow_account_creation(transactions[1])
        if company_code != 200:
            return company_answer, company_code
        
        # If everything went great, save the company to mongo instance
        self.companies_collection.insert_one({
            'name': company_name,
            'public_key': company_pub,
            'users': [],
            'jobs': []
        })

        return jsonify({
            'status': 'success',
            'msg': 'Company created successfully',
            'private_key': company_priv,
            'public_key': company_pub
        }), 200


    def __follow_domain_creation(self, transaction):
        """
        This method follows the creation of an iroha transaction
        that creates a domain.

        :param transaction: The Iroha transaction
        """
        for status in self.__iroha_network.tx_status_stream(transaction):
            print(status)
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({
                    'status': 'error',
                    'msg': 'Wrong company details set!'
                }), 403
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server error
                if status[2] == 1:
                    return jsonify({
                        'status': 'error',
                        'msg': 'Internal server error!'
                    }), 500
                # No such permission
                elif status[2] == 2:
                    return jsonify({
                        'status': 'error',
                        'msg': "This account doesn't have permissions to create a company!"
                    }), 401
                # Duplicate domain
                elif status[2] == 3:
                    return jsonify({
                        'status': 'error',
                        'msg': "This company is already registered!"
                    }), 409
                # No default role
                elif status[2] == 4:
                    return jsonify({
                        'status': 'error',
                        'msg': "There is no role specified as the default for this company!"
                    }), 403
            elif status[0] == "COMMITTED":
                return jsonify({
                    'status': 'success',
                    'msg': 'Company registered successfully!'
                }), 200


    def __follow_account_creation(self, transaction):
        """
        This method follows the status of a transaction that creates accounts.

        :param transaction: The Iroha transaction that creates an account
        """

        for status in self.__iroha_network.tx_status_stream(transaction):
            print(status)
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({
                    'status': 'error',
                    'msg': 'Wrong company details set!'
                }), 403
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server error
                if status[2] == 1:
                    return jsonify({
                        'status': 'error',
                        'msg': "Internal server error!"
                    }), 500
                # No such permissions
                elif status[2] == 2:
                    return jsonify({
                        'status': 'error',
                        'msg': "This account doesn't have the permissions to create a company!"
                    }), 401
                # No such domain
                elif status[2] == 3:
                    return jsonify({
                        'status': 'error',
                        'msg': "Couldn't create company and initial account!"
                    }), 404
                # Duplicate account
                elif status[2] == 4:
                    return jsonify({
                        'status': 'error',
                        'msg': "Initial account already exists in this company!"
                    }), 409
            elif status[0] == "COMMITTED":
                return jsonify({
                    'status': 'success',
                    'msg': 'Company created successfuly!'
                }), 200


    def cashTask(self, dev_id: str, task_id: str):
        """
        Burns a task token from the developer and transfers blockchain rewards to him
        """

        # Check task id exists and delete from Mongo
        dev_tasks = self.__orchestrator_db['users'].find_one({'_id': dev_id})['tasks']
        if not dev_tasks:
            return jsonify({'status': 'error', 'msg': 'Task was not found'}), 404
        
        dev_task = list(filter(lambda x: x['id']==task_id, dev_tasks))
        if not dev_task:
            return jsonify({'status': 'error', 'msg': 'Task was not found'}), 404
        
        # Save task as backup
        task = dev_task[0]

        # Delete task
        try:
            self.__orchestrator_db['users'].update_one({'_id': dev_id}, {"$pull": {"tasks": {"id": task_id}}})
        except:
            return jsonify({'status': 'error', 'msg': 'Task could not be deleted'}), 500

        # Get dev
        dev = self.__orchestrator_db['users'].find_one({'_id': dev_id})
        company = dev['current_job']['company'].split('@')[1]

        # Delete task token
        burn_task_tx = self.__iroha_client.transaction([
            self.__iroha_client.command(
                'SubtractAssetQuantity',
                asset_id="task#" + company,
                amount='1'
            )
        ])

        IrohaCrypto.sign_transaction(burn_task_tx, constants.private_key)
        self.__iroha_network.send_tx(burn_task_tx)

        burn_task_answer, burn_task_code = self.__follow_burn_task_tx(burn_task_tx)

        if burn_task_code != 200:
            self.__orchestrator_db['users'].update_one({'_id': dev_id}, {"$push": {'tasks': task}})
            return burn_task_answer, burn_task_code

        # Transfer rewards 

        rewards_txs = []

        # Determining amount
        amount = 0
        if task['difficulty'] == 'Hard':
            amount = 0.3
        elif task['difficulty'] == 'Medium':
            amount = 0.2
        elif task['difficulty'] == 'Easy':
            amount= 0.1

        for skill in task['skills']:
            rewards_txs.append(
                self.__iroha_client.transaction([
                    self.__iroha_client.command(
                        'TransferAsset',
                        src_account_id=constants.iroha_client,
                        dest_account_id=dev_id,
                        asset_id=skill + "#instrumentality",
                        description="rewards",
                        amount=str(amount)
                    )
                ])
            )
        
        self.__iroha_client.batch(rewards_txs, atomic=True)
        for tx in rewards_txs:
            IrohaCrypto.sign_transaction(tx, constants.private_key)
        self.__iroha_network.send_txs(rewards_txs)

        for tx in rewards_txs:
            tx_answer, tx_code = self.__follow_transfer_tx(tx)
            if tx_code != 200:
                return tx_answer, tx_code
        
        return jsonify({'status': 'success', 'msg': "Task completed. Rewards transfered."}), 200


    def __follow_burn_task_tx(self, transaction):
        for status in self.__iroha_network.tx_status_stream(transaction):
            if status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({'status': 'error', 'msg': "Task could not be marked as done"}), 500
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server error
                if status[2] == 1:
                    return jsonify({'status': 'error', 'msg': "Internal server error"}), 500
                # No such permission
                elif status[2] == 2:
                    return jsonify({'status': 'error', 'msg': "Orchestrator doesn't have permission to burn tasks"}), 403
                # No such asset
                elif status[2] == 3:
                    return jsonify({'status': 'error', 'msg': "Task asset was not found"}), 404
                # Not enough ballance
                elif status[2] == 4:
                    return jsonify({'status': 'error', 'msg': "Insufficient tasks minted by this company"}), 403
            elif status[0] == "COMMITTED":
                    return jsonify({'status': 'success', 'msg': "Task burned successfully"}), 200
    

    def __follow_transfer_tx(self, tx):
        for status in self.__iroha_network.tx_status_stream(tx):
            if status[0] == "REJECTED":
                return jsonify({'status': 'error', 'msg': "One or more assets could not be transfered to you!"}), 500
            elif status[0] == "STATELESS_VALIDATION_FAILED":
                return jsonify({'status': 'error', 'msg': "Rewards could not be transfered"}), 500
            elif status[0] == "STATEFUL_VALIDATION_FAILED":
                # Internal server error
                if status[2] == 1:
                    return jsonify({'status': 'error', 'msg': "Internal server error"}), 500
                # No such permission
                if status[2] == 2:
                    return jsonify({'status': 'error', 'msg': "Orchestrator doesn't have permission to trasnfer assets!"}), 403
                # No such source
                if status[2] == 3:
                    return jsonify({'status': 'error', 'msg': "Source account not found"}), 404
                # No such destination
                if status[2] == 4:
                    return jsonify({'status': 'error', 'msg': "Destination account not found"}), 404
                # No such asset
                if status[2] == 5:
                    return jsonify({'status': 'error', 'msg': "One asset was not found"}), 404
                # Not enough ballance
                if status[2] == 6:
                    return jsonify({'status': 'error', 'msg': "Orchestrator has run out of funds"}), 403
                # Too much to transfer
                if status[2] == 7:
                    return jsonify({'status': 'error', 'msg': "Can't transfer such large ammounts"}), 403
            elif status[0] == "COMMITTED":
                return jsonify({'status': 'success', 'msg': "Rewards have been transfered successfully!"}), 200


    def get_dev_wallet(self, account_id: str):

        wallet_query = self.__iroha_client.query(
            'GetAccountAssets',
            account_id=account_id
        )

        IrohaCrypto.sign_query(wallet_query, constants.private_key)
        iroha_response = self.__iroha_network.send_query(wallet_query)

        # Get response
        response = iroha_response.account_assets_response
        assets = []
        skills = []
        if response:
            assets = response.account_assets

            for asset in assets:
                skills.append({'asset_id': asset.asset_id, 'balance': asset.balance})

            return jsonify({'status': 'success', 'assets': skills}), 200
        else:
            return jsonify({'status': 'error', 'msg': "Couldn't retrieve any assets"}), 500
