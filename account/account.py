"""
This library handles account creation and management
"""

from iroha import Iroha, IrohaCrypto, IrohaGrpc
import json
from flask import jsonify
from binascii import Error
import redis
from orchestrator.orchestrator import OrchestratorHelper
from account.auth_helper import AuthHelper
import pymongo

# Private file with credentials
from private import constants

r = redis.Redis(constants.redis_network, '6379')
iroha_network = IrohaGrpc(constants.iroha_network)
iroha_client = Iroha(constants.iroha_client)
encoder = json.encoder.JSONEncoder()

auth_helper = AuthHelper()
orchestrator_helper = OrchestratorHelper()

# Mongo
mongo = pymongo.MongoClient(constants.mongo_instance)

def create(acc_name, domain_name):
    """
    This route creates a new private key, derives the public key and then sends it
    back to the requester in a json form.

    Parameters
    ----------
    acc_name : str
        The name of the account to be created. Must be unique.
    domain_name : str
        The name of the domain where the account should be register.

    Returns
    -------
    response : json
        A response containing the created keys.
    """
    private_key = IrohaCrypto.private_key().decode('utf-8')
    public_key = IrohaCrypto.derive_public_key(private_key).decode('utf-8')

    # Defining the account creation transaction
    new_account_tx = iroha_client.transaction([
        iroha_client.command(
            'CreateAccount',
            account_name=acc_name,
            domain_id=domain_name,
            public_key=public_key
        )
    ])

    # Signing the transaction with the instrumentality private key
    IrohaCrypto.sign_transaction(new_account_tx, constants.private_key)

    # Sending the transaction to be validated by the peers
    iroha_network.send_tx(new_account_tx)

    response = {}

    for status in iroha_network.tx_status_stream(new_account_tx):
        if status[0] == "STATEFUL_VALIDATION_FAILED":
            response['status'] = 'error'
            # Checking which error we got
            # First error: Couldn't create account. Internal error.
            if status[2] == 1:
                response['msg'] = "Couldn't create account. Internal error."
                response['code'] = "500"
                return encoder.encode(response), 500
            # Second error: No such permissions
            elif status[2] == 2:
                response['msg'] = "The node doesn't have permission to create accounts."
                response['code'] = "403"
                return encoder.encode(response), 403
            # Third error: No such domain
            elif status[2] == 3:
                response['msg'] = "There is no such domain."
                response['code'] = "404"
                return encoder.encode(response), 404
            # Fourth error: Account already exists
            elif status[2] == 4:
                response['msg'] = "Account with this name already exists."
                response['code'] = "403"
                return encoder.encode(response), 403
        elif status[0] == "COMMITTED":
            response['status'] = "success"
            response['private_key'] = private_key
            response['public_key'] = public_key
            response['msg'] = "Transaction written in ledger."
            response['code'] = 200
            return encoder.encode(response), 200


def auth(account_name, private_key):
    """
    This method is used to authenticate an existing user on the blockchain. Since Iroha expect an account_name when
    retrieving and account we first try to retrieve it be name. If the name doesn't exist we throw a 404 response.
    If the name exists we go ahead and check the private key. Since the account name is easily guessable or obtainable,
    we must use the private key to make sure that only the account owner can authenticate.

    :param str account_name: Unique name of the account
    :param str private_key:
    :return: JSON response, status code
    """

    # Gathering tools
    response = {}

    is_orchestrator = orchestrator_helper.verify(account_name)

    # Defining the query
    new_query = iroha_client.query(
        'GetSignatories',
        account_id=account_name
    )

    # Signing the query and sending it to network
    IrohaCrypto.sign_query(new_query, constants.private_key)
    iroha_response = iroha_network.send_query(new_query)
    keys = iroha_response.signatories_response.keys

    if not keys:
        response['status'] = 'error'
        response['msg'] = 'Account not found.'
        return encoder.encode(response), 404

    public_key = keys[0]

    # Verify if private key generates the same public key as the one from network
    try:
        if IrohaCrypto.derive_public_key(private_key).decode('utf-8') != public_key:
            response['status'] = 'error'
            response['msg'] = "Authentication failed. Private key did not match."
            return encoder.encode(response), 403
        else:
            response['status'] = 'success'
            response['msg'] = "Authentication successful."

            # Generate a token and save in redis alongside account_name
            # The token will expire in 1 day

            token = auth_helper.generate_token(account_name)
            response['token'] = token

            if is_orchestrator:
                response['orchestrator'] = True

            return encoder.encode(response), 200
    except Error as err:
        response['status'] = 'error'
        response['msg'] = str(err)
        return encoder.encode(response), 403


def verify(account_name, token):
    """
    This method verifies if some account can be authenticated in main application.
    A token is generated in the redis database after a successful verification of the
    private key.

    If that token is still in the database (it hasn't expired) then allow authentication.

    :param str account_name:  The unique name of the account on the chain.
    :param str token: A token string
    :return: json
    """

    if auth_helper.validate_token(account_name, token):
        return jsonify({'msg': 'Authorization succeeded', 'status': 'success'}), 200
    else:
        return jsonify({'msg': 'Not authorized to access this route', 'status': 'error'}), 401


def set_details(body):
    """
    This method will set the details of an account
    :param dict body: The body of the http request with the data to set
    :return: json response, status code
    """
    response = {}

    token = body['token']
    account = body['account']
    info = body['info']

    # Checking token for validity
    if not auth_helper.validate_token(account, token):
        return jsonify({'status': 'error', 'msg': 'Session expired or invalid credentials'}), 401

    try:
        mongo['orchestrator_db']['users'].insert_one({
            '_id': account,
            'description': info['description']
        })
    except:
        mongo['orchestrator_db']['users'].update_one({'_id': account}, {"$set": {'description': info['description']}})

    # Creating transaction
    new_tx = iroha_client.transaction([
        iroha_client.command(
            'SetAccountDetail',
            account_id=account,
            key='firstname',
            value=info['firstname']
        ),
        iroha_client.command(
            'SetAccountDetail',
            account_id=account,
            key='surname',
            value=info['surname']
        ),
        iroha_client.command(
            'SetAccountDetail',
            account_id=account,
            key='phone',
            value=info['phone']
        ),
        iroha_client.command(
            'SetAccountDetail',
            account_id=account,
            key='email',
            value=info['email']
        ),
        iroha_client.command(
            'SetAccountDetail',
            account_id=account,
            key='city',
            value=info['city']
        ),
        iroha_client.command(
            'SetAccountDetail',
            account_id=account,
            key='study',
            value=info['study']
        )
    ])

    # Sign transaction as Instrumentality
    IrohaCrypto.sign_transaction(new_tx, constants.private_key)

    # Send transaction to network for processing
    iroha_network.send_tx(new_tx)

    # Checking for errors from network
    for status in iroha_network.tx_status_stream(new_tx):
        if status[0] == "STATEFUL_VALIDATION_FAILED":
            response['status'] = 'error'
            # Checking which error we got
            # 1. Internal error
            if status[2] == 1:
                response['msg'] = "Internal error. Contact developers or try again."
                return encoder.encode(response), 500
            # Permission error
            elif status[2] == 2:
                response['msg'] = "This instance doesn't have permissions to modify account details."
                return encoder.encode(response), 403
            # No such account
            elif status[2] == 3:
                response['msg'] = "No account with this id was found."
                return encoder.encode(response), 404
        elif status[0] == "COMMITTED":
            response['status'] = "success"
            response['msg'] = "Account details set."
            return encoder.encode(response), 200


def get_details(account, token):
    """
    This method retrieves the details of an account
    :param str account:
    :param str token:
    :return: Json response, status
    """

    response = {}

    if not auth_helper.validate_token(account, token):
        return jsonify({'status': 'error', 'msg': 'Invalid token'}), 401
    else:
        # Write a new query for the blockchain
        new_query = iroha_client.query(
            'GetAccount',
            account_id=account
        )

        # Sign the query with the Instrumentality key
        IrohaCrypto.sign_query(new_query, constants.private_key)

        # Send the query to the network and get an answer
        iroha_response = iroha_network.send_query(new_query)
        account_details = iroha_response.account_response.account.json_data

        decoder = json.decoder.JSONDecoder()

        try:
            info = decoder.decode(account_details)[constants.iroha_client]
        except KeyError:
            return jsonify({'msg': 'No data found', 'status': 'error'}), 404

        try:
            user = mongo['orchestrator_db']['users'].find_one({'_id': account})
            info['description'] = user['description']
        except:
            print('Unable to load description for ' + account)


        return info, 200
