from redis import Redis
from private import constants
from secrets import token_hex
from datetime import timedelta


class AuthHelper:
    """
    A class with portable functions that help with authentication/authorization on the Instrumentality
    platform.

    The redis instance associated with this object is started on port '6379' ny default. Pass the 'redis_port'
    parameter to set a custom port.
    """
    def __init__(self, redis_port=None):
        if redis_port:
            self.__redis = Redis(constants.redis_network, str(redis_port))
        else:
            self.__redis = Redis(constants.redis_network, '6379')

    def generate_token(self, account: str):
        """
        This method generates a new 32-bit secure token and saves it in
        the redis db along the account id.

        :param str account: The account id for which the token is generated
        :return: token or error from redis
        """
        token = token_hex(32)
        try:
            self.__redis.setex(account, timedelta(hours=5), token)
        except Exception as ex:
            raise ex

        return token

    def get_token(self, account: str) -> str:
        """
        Returns the token corresponding to the account id, if it exists in the redis db.
        There are two reasons why this transaction may yield an empty string:

        1. The token has never been introduced in the redis db.

        2. The token has been revoked either manually or due to the expiration time.

        :param str account: The unique account id to search for.
        :return: An empty string or the token associated with the account
        """

        token = self.__redis.get(account)
        if not token:
            return ''
        else:
            return token.decode('utf-8')

    def validate_token(self, account: str, token: str) -> bool:
        """
        Compares a given token for an account with the token that's in the redis db. This method returns False if
        there's no token in redis or that token is not the same

        :param str account: The account to which the token is supposed to belong
        :param str token: The token to check
        :return: True or False
        """

        redis_token = self.get_token(account)

        if redis_token == '':
            return False
        elif redis_token != token:
            return False

        return True
